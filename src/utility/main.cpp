#include <iostream>
#include <cstdlib>

#include "n_chain.hpp"



KovStringChain<3> ch;

int main(int argc, char** argv) {
    std::string line;
    unsigned int amount = 1;
    unsigned long int linesParsed = 0;

    while (std::cin.good()) {
        getline(std::cin, line, '\n');

        linesParsed++;
        std::cerr << linesParsed << "\r";

        ch.Parse(line, " \t\n,.!;-'\"+-*/$()");
    }

    if (argc > 1)
        amount = atoi(argv[1]);

    // std::cerr << "[ Generating " << amount << " lines of MCMC output... ]\n";

    for (unsigned short i = 0; i < amount; i++) {
        KovData<std::string, 3> res = ch.Generate(200);
        std::cout << "\"" << (res.Valid() ? res.GetData() : "") << "\"\n";
    }

    return 0;
}