#pragma once

#include <vector>
#include <random>
#include <iostream>
#include <utility>
#include <string>

#include "n_node.hpp"



typedef std::mt19937 mtwist;

template <typename T, unsigned char order>
class KovChain {
private:
    mtwist chainRNG;

    void _init(void);

protected:
    KovNode<T, order> start;
    KovNode<T, order> end;
    KovDataAppender<T> *appender;

public:
    virtual KovDataAppender<T> *GetAppender(void) { return appender; }

    typename std::deque<KovNode<T, order>> nodes;
    std::vector<std::shared_ptr<KovEdge<T, order>>> allEdges;

    KovChain(long seed = -1);
    KovChain(KovDataAppender<T> &appender, long seed = -1);
    ~KovChain();

    virtual KovNode<T, order> *MakeNode(T &wrapped);
    virtual void AddLink(std::array<KovNode<T, order>*, order> *a, KovNode<T, order> *b, T &sep);
    virtual KovData<T, order> Generate(unsigned long maxLen);
};

template<unsigned char order>
class KovStringChain : public KovChain<std::string, order> {
private:
    std::pair<std::string, std::string> ReadFrom(std::string *data, const std::string &delims);

public:
    void Parse(std::string data, std::string delims);
    KovDataAppender<std::string> *GetAppender(void) { return &KovStringInit; }
};



//----------------//
// IMPLEMENTATION //
//----------------//

#include <random>
#include <ctime>
#include <cassert>



typedef std::mt19937 mtwist;

template <typename T, unsigned char order>
KovChain<T, order>::KovChain(long seed) {
    std::time_t t = std::time(0);

    chainRNG.seed(seed >= 0 ? seed : t);

    _init();
}

template <typename T, unsigned char order>
KovChain<T, order>::KovChain(KovDataAppender<T> &appender, long seed) {
    this->appender = &appender;

    _init();
}

template <typename T, unsigned char order>
void KovChain<T, order>::_init(void) {
}


template <typename T, unsigned char order>
KovChain<T, order>::~KovChain() {
    // pass
}

template <typename T, unsigned char order>
KovData<T, order> KovChain<T, order>::Generate(unsigned long maxLen) {
    KovNode<T, order> *cur = &this->start;
    KovData<T, order> result(GetAppender());

    while (cur && cur != &end && maxLen--) {        
        unsigned long maxFreq = 0;

        for (typename std::deque<std::shared_ptr<KovEdge<T, order>>>::iterator it = cur->out.begin(); it != cur->out.end(); it++) {
            if (!(*it)->good) continue;
            maxFreq += (*it)->frequency;
        }

        std::uniform_int_distribution<unsigned long> freqPosRange(0, maxFreq);
        unsigned long freqPos = freqPosRange(chainRNG);
        KovEdge<T, order>* edge;

        for (typename std::deque<std::shared_ptr<KovEdge<T, order>>>::iterator it = cur->out.begin(); it != cur->out.end(); it++) {
            edge = &**it;

            if (!edge->good) continue;

            unsigned short fq = edge->frequency;

            if (fq > freqPos) {
                freqPos = 0;
                break;
            }

            freqPos -= fq;
        }


        assert(freqPos == 0);

        result = result + *(edge->separator) + *(edge->to);

        cur = edge->to;
    }

    return result;
}

template <typename T, unsigned char order>
void KovChain<T, order>::AddLink(std::array<KovNode<T, order>*, order> *a, KovNode<T, order> *b, T &sep) {
    bool exists = false;
    KovEdge<T, order> *ed;

    for (typename std::deque<std::shared_ptr<KovEdge<T, order>>>::iterator it = a->out.begin(); it != a->out.end(); it++)
        if ((*it)->good && (*it)->to == b && (*it)->separator->GetData() == sep) {
            exists = true;
            (*it)->frequency++;
            // ed = *it;
        }

    if (!exists) {
        KovEdge<T, order> *edge = new KovEdge<T, order>(GetAppender(), a, b, new T(sep));
        allEdges.push_back(std::shared_ptr<KovEdge<T, order>>(edge));

        a->out.push_back(allEdges.back());
        b->in.push_back(allEdges.back());

        // ed = &allEdges.back();
    }

    // std::cerr << "(" << (a->GetData() ? *(a->GetData()) : "(NULL)") << ") => (" << (b->GetData() ? *(b->GetData()) : "(NULL)") << ") ...x" << ed->frequency << "\n";
}


template <typename T, unsigned char order>
KovNode<T, order> *KovChain<T, order>::MakeNode(T &wrapped) {
    for (typename std::deque<KovNode<T, order>>::iterator it = nodes.begin(); it != nodes.end(); it++) {
        if (!((*it).GetData())->compare(wrapped))
            return &*it;
    }

    KovNode<T, order> res(GetAppender(), new T(wrapped));
    nodes.push_back(res);

    return &nodes.back();
}


template <unsigned char order>
std::pair<std::string, std::string> KovStringChain<order>::ReadFrom(std::string *data, const std::string &delims) {
    std::string token("");
    std::string separator("");

    std::string::iterator it = data->begin();

    char ch[2] = { 0, 0 };
    unsigned short i = 0;

    for (; it != data->end(); it++) {
        if (delims.find_first_of(*it) == -1) {
            ch[0] = *it;
            token.append(ch);
            i++;
        }
        
        else
            break;
    }

    for (; it != data->end(); it++) {
        if (delims.find_first_of(*it) != -1) {
            ch[0] = *it;
            separator.append(ch);
            i++;
        }
        
        else
            break;
    }

    *data = data->substr(i);

    return std::pair<std::string, std::string>(token, separator);
}

std::string str_empty = "";

template <unsigned char order>
void KovStringChain<order>::Parse(std::string data, std::string delims) {
    std::array<KovNode<std::string, order>*, order> *ctx;
    KovNode<std::string, order> *next, *cur = &this->start;

    for (unsigned char i = 0; i < order; i++)
        ctx[i] = NULL;

    while (data.size()) {
        std::pair<std::string, std::string> tokenDelim = ReadFrom(&data, delims);

        if (!tokenDelim.first.size()) {
            this->AddLink(cur, &this->end, tokenDelim.second);
            return;
        }

        next = this->MakeNode(tokenDelim.first);
        this->AddLink(cur, next, tokenDelim.second);

        cur = next;
    }

    this->AddLink(cur, &this->end, str_empty);
}