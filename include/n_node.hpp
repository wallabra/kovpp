#pragma once

#include <vector>
#include <deque>
#include <memory>

#include "n_data.hpp"
#include "n_edge.hpp"



template <typename T, unsigned char order>
class KovEdge;


template <typename T, unsigned char order>
class KovChain;


template <typename T, unsigned char order>
class KovNode {
protected:
    std::array<std::shared_ptr<KovData<T, order>>, order> *ctx;

public:
    std::deque<std::shared_ptr<KovEdge<T, order>>> in;
    std::deque<std::shared_ptr<KovEdge<T, order>>> out;
    
    unsigned long long int id;

    KovNode(const KovNode<T, order> &other);
    KovNode(KovData<T, order> &initData);
    KovNode(KovData<T, order> *initData);
    KovNode(KovDataAppender<T> *appender, T &initData);
    KovNode(KovDataAppender<T> *appender, T *initData);
    KovNode();

    ~KovNode();

    T *GetData(void);
    std::shared_ptr<KovData<T, order>> GetWrappedData(void);
    unsigned long long int GetID(void);

    KovData<T, order> operator+(KovNode<T, order> &other) {
        return data->Append(other.data);
    }

    KovData<T, order> operator+(KovData<T, order> &other) {
        return data->Append(other);
    }

    class SafePointer {
        KovNode<T, order>* _ptr;
        unsigned short _refs;
        std::vector<KovNode<T, order>::SafePointer*> _links;

    public:
        SafePointer(KovNode<T, order> *nod);
        SafePointer(KovNode<T, order>::SafePointer &ptr);

        void operator~() {
            if (!--(_refs))
                delete _ptr;

            for (typename std::deque<KovNode<T, order>::SafePointer*>::iterator it = _links.begin(); it < _links.end(); it++) 
                (*it)->_refs--;
        }

        KovNode<T, order> operator*(void) {
            assert(_refs > 0);
            return *_ptr;
        }

        KovNode<T, order> *Pointer(void) {
            assert(_refs > 0);
            return _ptr;
        }
    };

    typename KovNode<T, order>::SafePointer *MakeSafePointer(void);
};

//----------------//
// IMPLEMENTATION //
//----------------//

#include <cassert>
#include <iostream>



static unsigned long long int Kov_NodeIDCounter = 0;


template <typename T, unsigned char order>
T *KovNode<T, order>::GetData(void) {
    if (!data) return NULL;
    return &(data->GetData());
}

template <typename T, unsigned char order>
std::shared_ptr<KovData<T, order>> KovNode<T, order>::GetWrappedData(void) {
    return data;
}

template <typename T, unsigned char order>
unsigned long long int KovNode<T, order>::GetID(void) {
    return id;
}


template <typename T, unsigned char order>
KovNode<T, order>::KovNode(const KovNode<T, order> &other) : data(new KovData<T, order>(*other.data)), id(Kov_NodeIDCounter++) {}

template <typename T, unsigned char order>
KovNode<T, order>::KovNode(KovData<T, order> &initData) : data(&initData), id(Kov_NodeIDCounter++) {}

template <typename T, unsigned char order>
KovNode<T, order>::KovNode(KovData<T, order> *initData) : data(initData), id(Kov_NodeIDCounter++) {}

template <typename T, unsigned char order>
KovNode<T, order>::~KovNode() {
    for (typename std::deque<std::shared_ptr<KovEdge<T, order>>>::iterator it = out.begin(); it != out.end(); it++)
        (*it)->good = false;

    for (typename std::deque<std::shared_ptr<KovEdge<T, order>>>::iterator it = in.begin(); it != in.end(); it++)
        (*it)->good = false;
}

template <typename T, unsigned char order>
KovNode<T, order>::KovNode(KovDataAppender<T> *appender, T &initData) : id(Kov_NodeIDCounter++), data(new KovData<T, order>(appender, initData)) {}   

template <typename T, unsigned char order>
KovNode<T, order>::KovNode(KovDataAppender<T> *appender, T *initData) : id(Kov_NodeIDCounter++), data(new KovData<T, order>(appender, initData)) {}   

template <typename T, unsigned char order>
KovNode<T, order>::KovNode() : data(NULL), id(Kov_NodeIDCounter++) {}

template <typename T, unsigned char order>
KovNode<T, order>::SafePointer::SafePointer(KovNode<T, order> *nod) : _ptr(nod), _refs(1) {
    assert(_ptr != NULL);
}

template <typename T, unsigned char order>
KovNode<T, order>::SafePointer::SafePointer(KovNode<T, order>::SafePointer &ptr) {
    ptr._refs += 1;
    _refs = ptr._refs;

    _links.push_back(&ptr);
    ptr._links.push_back(this);
}

template <typename T, unsigned char order>
typename KovNode<T, order>::SafePointer *KovNode<T, order>::MakeSafePointer(void) {
    return new KovNode<T, order>::SafePointer(this);
}