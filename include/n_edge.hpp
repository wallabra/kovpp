#pragma once

#include "n_data.hpp"
#include "n_node.hpp"



template <typename T, unsigned char order>
class KovNode;


template <typename T, unsigned char order>
class KovChain;


template <typename T, unsigned char order>
class KovEdge {
public:
    std::array<KovNode<T, order>*, order> *from;
    KovNode<T, order> *to;
    KovData<T, order> *separator;
    unsigned short frequency;
    bool good;

    KovEdge(std::array<KovNode<T, order>*, order> *f, KovNode<T, order> *t, KovData<T, order> *s) : from(f), to(t), separator(s), frequency(1), good(true) {}
    KovEdge(KovDataAppender<T> *a, std::array<KovNode<T, order>*, order> *f, KovNode<T, order> *t, T &s) : from(f), to(t), separator(new KovData<T, order>(a, s)), frequency(1), good(true) {}
    KovEdge(KovDataAppender<T> *a, std::array<KovNode<T, order>*, order> *f, KovNode<T, order> *t, T *s) : from(f), to(t), separator(new KovData<T, order>(a, s)), frequency(1), good(true) {}

    ~KovEdge() {};

    T Represent(KovDataAppender<T> &appender);
};

//----------------//
// IMPLEMENTATION //
//----------------//

#include <cassert>



template <typename T, unsigned char order>
T KovEdge<T, order>::Represent(KovDataAppender<T> &appender) {
    T res;

    assert(from != NULL);
    assert(to != NULL);

    if (from->GetData())
        res = from->GetData();

    if (separator->GetData())
        res = (*appender)(res, separator->GetData());

    if (to->GetData())
        res = (*appender)(res, to->GetData());

    return res;
}